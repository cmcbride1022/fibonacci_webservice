// Web client class using Jersey framework to test Fibonacci web service
// Author: Carter McBride
// January 29th, 2016

package com.virtustream.fibonacci;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

public class FibonacciTester {
	
	private Client client;
	private String REST_SERVICE_URL = "http://localhost:8080/Fibonacci/fibservice";
	private static final String PASS = "PASS";
	private static final String FAIL = "FAIL";
	
	private static List<Integer> test_vals; // random sequence lengths to test
	private static ArrayList<Integer> fib_vals; // correct Fibonacci sequence
	private static final int max = 15;
	private static final int min = -5;
	
	private void init() {
		Random rand = new Random();
		this.client = ClientBuilder.newClient();
		test_vals = new ArrayList<Integer>();
		
		// correct Fibonacci values to compare with the sequence returned by the web service
		fib_vals = new ArrayList<Integer>(Arrays.asList(0,1,1,2,3,5,8,13,21,34,55,89,144,233,377));
		
		// generate 10 random sequence lengths to test
		int rand_int;
		for (int i = 0; i < 10; i += 1) {
			rand_int = rand.nextInt((max - min) + 1) + min;
			test_vals.add(rand_int);
		}
		
	}
	
	public static void main(String[] args) {
		FibonacciTester tester = new FibonacciTester();
		
		tester.init();
		tester.testGetFib();
	}
	
	private void testGetFib() {
		
		// Fibonacci sequence returned by web service
		String result;
		
		for(int num : test_vals) {
			
			// retrieve the returned string
			result = client.target(REST_SERVICE_URL).path("/gen/" + num).request(MediaType.TEXT_HTML).get(String.class);
			
			// first check for a negative length -> not possible, so just ensure the correct error message was returned
			if (num < 0) {
				if (result.equals("Error: Number cannot be negative")) {
					System.out.println("Test value: " + num + "\tResult: " + PASS + "\tSequence: N/A");
					continue;
				}
			}
			
			// extract Fibonacci sequence from the rest of the text returned from the web service
			result = result.substring(result.indexOf(":") + 2, result.length());
			
			// sequences are the same -> web service generated the correct sequence
			if (result.equals(fib_vals.subList(0, num).toString())) {
				
				// print the sequence length, the sequence itself, and the passing result of the test
				System.out.println("Test value: " + num + "\tResult: " + PASS + "\tSequence: " + result);
			}
			
			// there is a mismatch in the sequences -> indicates an error with the web service
			else {
				
				// print the failing test result and both sequences for examination
				System.out.println("Test value: " + num + "\tResult: " + FAIL);
				System.out.println("Generated sequence: " + result);
				System.out.println("Correct sequence" + fib_vals.subList(0, num).toString());
			}
		}

	}
	
	
}