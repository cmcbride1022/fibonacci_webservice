// Fibonacci calculation class
// Author: Carter McBride
// January 29th, 2016

package com.virtustream.fibonacci;

import java.util.ArrayList;

public class Fibonacci {

	private ArrayList<Integer> fib_sequence; // this list contains the final Fibonacci sequence
	private int length; // length of sequence requested by client
	
	public Fibonacci(int length) {
		this.length = length;
	}
	
	// construct a list containing the appropriate length Fibonacci sequence
	public ArrayList<Integer> calculate() {
		fib_sequence = new ArrayList<Integer>(length);
			
		for (int i = 0; i < length; i += 1) {
			
			// compute and append the next value to the list
			fib_sequence.add(fibonacci(i));
		}
		return fib_sequence;
	}
	
	// recursively compute the Fibonacci sequence of length val
	private Integer fibonacci(int val) {
		if (val <= 1) return val; // base case
		
		return fibonacci(val-1) + fibonacci(val-2);
	}

	public ArrayList<Integer> getFib_sequence() {
		return fib_sequence;
	}
	
	public int getLength() {
		return length;
	}
	
}
