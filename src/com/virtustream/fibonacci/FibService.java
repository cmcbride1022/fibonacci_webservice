// Fibonacci service class
// Author: Carter McBride
// January 29th, 2016

package com.virtustream.fibonacci;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/* This class directs incoming requests to the appropriate method
 * to calculate the first n Fibonacci numbers
 */
@Path("/fibservice")
public class FibService {
	
	Fibonacci fib;
	String sequence;
	
	@GET
	@Path("/gen/{num}")
	@Produces(MediaType.TEXT_HTML)
	public String getFib(@PathParam("num") int num) {
		
		// input number cannot be negative -> print an error message
		if (num < 0) { return "Error: Number cannot be negative"; }
		
		// initialize new Fibonacci object with a sequence length of num
		fib = new Fibonacci(num);
		
		// generate the appropriate sequence
		sequence = fib.calculate().toString();
		
		if (num == 1) { return "The first Fibonacci number is: " + sequence; }
		else { return "The first " + num + " Fibonacci numbers are: " + sequence; }
	}

}
